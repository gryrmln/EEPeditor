About gryrmln/EEPeditor
=======================

An Editor to alter the configuration settings stored in Intel Hex EEP files
prior to flashing to an AVR.

This software is currently targeted at editing eep files for writing to the
QRP Labs (qrp-labs) range of U3S kits but may be expanded at a later date.

This is not official QRP Labs code, and is not supported directly by QRP Labs
and has no affiliation with QRP Labs whatsoever. Any issues with this software
should be reported to the software author.


**Installing gryrmln/EEPeditor**

Most of the instructions are related to installing the support software
which is not part of EEPeditor and may already be available on your system.

Please report any issues you have with installation or operation.

<br>

Installation instructions are provided as follows:

    *Windows        INSTALL_windows.md

    *MacOS          INSTALL_macos.md

    *Debian         INSTALL_debian.md


