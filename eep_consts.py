#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim:ai:sw=4:ts=8:et:fileencoding=utf-8
#
# EEP Consts 1v2
#
# Copyright (C) 2018 Gray Remlin 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Global Constants for use across modules """

VERSION = 1.2

# Dummy: offset, field size
TYPDMY0 = 0
# Version: offset, field size
TYPVER0 = 1
# Integer: offset, field size
TYPINT0 = 2
# Text String : offset, field size
TYPTXT0 = 3
# Text String (zero terminated): offset, field size
TYPTXZ0 = 4
# Compound: offset, field size, baselist, labellist
TYPCPD0 = 5
# Transmission slot: offset, field size, modedict, labellist
TYPSLT0 = 6
# Character set
TYPCHR0 = 7

def main_constants():
    """ Display Global Constants
    functional invocation required to avoid globals() resize whilst parsing."""
    print("EEP Consts {}\n".format(VERSION))
    for name in globals():
        print("{}\t{} = {}".format(type(name), name, eval(name)))
    return

if __name__ == '__main__':
    help(__name__)
    main_constants()
