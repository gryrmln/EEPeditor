
Installing gryrmln/EEPeditor
============================

<br>

EEPeditor has the following dependancies:

Python3

Python3 Tk/Tcl support

Python3 IntelHex support

<br>

**Install dependency Python3 with Tk/Tcl and pip support**

Get the latest Python 3.x.x from

    https://www.python.org/downloads/

Or current latest direct link (dependant on 32Bit or 64Bit Windows):

    https://www.python.org/ftp/python/3.6.5/python-3.6.5.exe

    https://www.python.org/ftp/python/3.6.5/python-3.6.5-amd64.exe


**Install dependency Python3 IntelHex**

*IntelHex can be either installed via use of:*

  * Python Package Manager 'pip' (recommended)
  * Git
  * Direct download Zip file


*Install dependency Python3 IntelHex using pip*

Install IntelHex

    > py -m pip install intelhex


*Install dependency Python3 IntelHex using Git*

Install Git

    > https://git-scm.com/download/win

Install IntelHex

    > git clone https://github.com/bialix/intelhex.git intelhex.git

    > cd intelhex.git

    > py setup.py install


*Install dependency Python3 IntelHex using zip download*

Navigate to the following URL

    https://github.com/bialix/intelhex.git

    and click on 'Clone or Download' then 'ZIP'

After unzipping

    > cd intelhex.git

    > py setup.py install


**Install EEPeditor**

*EEPeditor can be either installed via use of:*

  * Git (recommended)
  * Direct download Zip file


*Install Python3 EEPeditor using git*

Install Git

    > https://git-scm.com/download/win

Install Python3 EEPeditor

    > git clone https://github.com/gryrmln/EEPeditor.git EEPeditor.git


*Install Python3 EEPeditor using zip download*

Navigate to the following URL

    https://github.com/gryrmln/EEPeditor.git

    and click on 'Clone or Download' then 'ZIP'

After unzipping


**To run EEPeditor**

    > cd EEPeditor.git

    > py eep_editor.py 'optional eep filename'


*To download latest EEPeditor Updates using git*

    > cd  EEPeditor.git

    > git pull

