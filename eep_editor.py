#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim:ai:sw=4:ts=8:et:fileencoding=utf-8
#
#  EEPeditor 1v2
#
# Copyright (C) 2018 Gray Remlin 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import json
from intelhex import IntelHex
from eep_consts import *

from tkinter import *
from tkinter import messagebox
from tkinter import filedialog
from tkinter import simpledialog


class up_int:

    def __init__(self, var, ih, var_info, digitset):
        self._var = var
        self._ih = ih
        self._var_info = var_info
        self._digitset = digitset
        # FIXME: investigate 'validate' and 'textvariable'
        self._var.trace("w", self._auto_range)
        return

    def _auto_range(self, *arg):
        val = self._var.get()
        if val:
            newval = ""
            for c in val:
                if c in self._digitset:
                    #print("c '{}'".format(c))
                    newval += c
            #print("newval '{}'".format(newval))
            self._var.set(newval)
        return

    def _int2bytes(self, num, val):
        # Integer LSB to MSB
        byts = []
        div = 1
        # byts.append(val % 256)
        # byts.append(int(val / 256) % 256)
        # byts.append(int(val / 65536) % 256)
        # byts.append(int(val / 16777216) % 256)
        for i in range(0, num):
            byts.append(int(val / div) % 256)
            div *= 256
        return byts

    def do_event(self, event=None):
        val = int(self._var.get())
        byts = self._int2bytes(self._var_info[2], val)
        offset = self._var_info[1]
        self._ih.frombytes(byts, offset)
        #print("Integer {}".format(byts))
        print("{} {}".format(self._var_info[3], val))
        return


class up_asciisz:

    def __init__(self, var, ih, var_info, entry_str, charset):
        self._var = var
        self._ih = ih
        self._var_info = var_info
        self._entry_str = entry_str
        self._charset = charset
        self._var.trace("w", self._auto_range)
        return

    def _auto_range(self, *arg):
        val = self._var.get().upper()[:self._var_info[2]-1]
        if val:
            newval = ""
            for c in val:
                if c in self._charset:
                    #print("c '{}'".format(c))
                    newval += c
            #print("newval '{}'".format(newval))
            self._var.set(newval)
        return

    # event is Enter Key
    def do_event(self, event=None):
        #print("event = {}".format(event))
        val = self._var.get().upper()
        self._ih.putsz(self._var_info[1], val)
        self._entry_str.delete(0, END)
        self._entry_str.insert(0, val)
        #print("String {}".format(val))
        print("{} {}".format(self._var_info[3], val))
        return

class up_compound:

    def __init__(self, idx, varlist, ih, var_info, digitset):
        self._idx = idx
        self._varlist = varlist
        self._var = varlist[idx]
        self._ih = ih
        self._var_info = var_info
        self._digitset = digitset
        self._var.trace("w", self._auto_range)
        return

    def _auto_range(self, *arg):
        val = self._var.get()
        if val:
            newval = ""
            for c in val:
                if c in self._digitset:
                    #print("c '{}'".format(c))
                    newval += c
            #print("newval '{}'".format(newval))
            self._var.set(newval)
        return

    # Unused
    def _bytes2int(self, num, byts):
        val = 0
        for i in range(0, num):
            val = (val * 256) + byts[i]
        return val

    def _int2bytes(self, num, val):
        # Integer LSB to MSB
        byts = []
        div = 1
        # byts.append(val % 256)
        # byts.append(int(val / 256) % 256)
        # byts.append(int(val / 65536) % 256)
        # byts.append(int(val / 16777216) % 256)
        for i in range(0, num):
            byts.append(int(val / div) % 256)
            div *= 256
        return byts

    # reconstruct compound integer from all fields
    # event is Enter Key
    def do_event(self, event=None):
        val = 0
        for i in range(0, len(self._var_info[3])):
            val += (int(self._varlist[i].get()) * self._var_info[3][i])
        val += int(self._varlist[-1].get())
        byts = self._int2bytes(self._var_info[2], val)
        offset = self._var_info[1]
        self._ih.frombytes(byts, offset)
        #print("Compound {}".format(self._ih[self._var_info[1]]))
        print("{} {}:{}".format(self._var_info[5],
                        self._var_info[4][self._idx], self._varlist[self._idx].get()))
        return

#class up_asciisz_box:
#
#    def __init__(self, var, ih, var_info):
#        self._var = var
#        self._ih = ih
#        self._var_info = var_info
#        return
#
#    def do(self):
#        ttl = 'Edit Text'
#        prmpt = 'Edit'
#        response = simpledialog.askstring(ttl, prmpt, initialvalue=self._var.get())
#        if response == None:
#            # user hit cancel
#            return
#        val = response.upper()
#        self._var.set(val)
#        self._ih.putsz(self._var_info[1], val)
#        print("{}".format(val))
#        return

class SlotRow:


    _hextable = { '1':1, '2':2, '3':3, '4':4, '5':5, '6':6, '7':7, '8':8,
                    '9':9, '0':0, 'A':10, 'B':11, 'C':12, 'D':13, 'E':14, 'F':15 }

    _dBm_powers = [ 0, 3, 7, 10, 13, 17, 20, 23, 27, 30, 33, 37, 40, 43, 47, 50, 53, 57 ]

    def __init__(self, ih, var_info, slot, frame):
        self._ih = ih
        self._var_info = var_info
        self._slot = slot
        self._slot_offset = var_info[1] + (slot * 8)

        slotrow = slot+1

        vlabel = Label(frame, text="{:X}".format(slot), foreground='black', background='grey')
        vlabel.grid(row=slotrow, column=0)
        self._enabled = IntVar()
        self._button_enabled = Checkbutton(frame, text='', onvalue=1, offvalue=0,
                                    variable=self._enabled, command=self._update_enabled)
        self._button_enabled.grid(row=slotrow, column=1, sticky='w')
        if self._ih[self._slot_offset] & 0x80:
            self._button_enabled.select()
        else:
            self._button_enabled.deselect()

        self._band = StringVar()
        self._entry_band = Entry(frame, textvariable=self._band, width=2, justify=RIGHT)
        self._entry_band.bind('<Key-Return>', self._update_band)
        self._entry_band.grid(row=slotrow, column=2)
        # set initial value before setting validation callback
        slot_band = self._ih[self._slot_offset+1]
        self._entry_band.insert(0, str(slot_band))
        validate_func = (frame.register(self._validate_band), '%d', '%S')
        self._entry_band.configure(validate='all', validatecommand=validate_func)

        self._frequency = StringVar()
        self._entry_frequency = Entry(frame, textvariable=self._frequency,
                                                        width=12, justify=RIGHT)
        self._entry_frequency.bind('<Key-Return>', self._update_frequency)
        self._entry_frequency.grid(row=slotrow, column=3)
        # set initial value before setting validation callback
        start = self._slot_offset + 4
        finish = start + 4
        t = 0
        # LSB first, reverse parse to simplify math
        for i in range(finish-1, start-1, -1):
            t = t * 256 + self._ih[i]
        slot_freq = str(t)
        self._entry_frequency.insert(0, slot_freq)
        validate_func = (frame.register(self._validate_frequency), '%d', '%S')
        self._entry_frequency.configure(validate='all', validatecommand=validate_func)

        self._modelb = Listbox(frame, height=1, width=8)
        self._modelb.grid(row=slotrow, column=4)
        self._modelb.bind('<Key-Return>', self._update_mode)
        modes = self._var_info[3]
        for mode in modes:
            self._modelb.insert(END, modes[mode])
        slot_mode_str = str(self._ih[self._slot_offset] & 0x7F)
        if slot_mode_str in modes:
            self._modelb.see(int(slot_mode_str))
        else:
            print("Mode Table Error")

        self._pm = StringVar()
        self._entry_pm = Entry(frame, textvariable=self._pm, width=2, justify=RIGHT)
        self._entry_pm.bind('<Key-Return>', self._update_pm)
        self._entry_pm.grid(row=slotrow, column=5)
        # set initial value before setting validation callback
        slot_pm = self._ih[self._slot_offset+3]
        self._entry_pm.insert(0, str(slot_pm))
        validate_func = (frame.register(self._validate_pm), '%d', '%S')
        self._entry_pm.configure(validate='all', validatecommand=validate_func)

        self._aux = StringVar()
        self._entry_aux = Entry(frame, textvariable=self._aux, width=2, justify=RIGHT)
        self._entry_aux.bind('<Key-Return>', self._update_aux)
        self._entry_aux.grid(row=slotrow, column=6)
        # set initial value before setting validation callback
        slot_aux = "{}".format(self._ih[self._slot_offset+2])
        self._entry_aux.insert(0, slot_aux)
        validate_func = (frame.register(self._validate_aux), '%d', '%S')
        self._entry_aux.configure(validate='all', validatecommand=validate_func)

        return

    def _update_enabled(self):
        if self._enabled.get():
            self._ih[self._slot_offset] |= 0x80
        else:
            self._ih[self._slot_offset] &= ~0x80
        print("{:X}] {} = {}".format(self._slot, self._var_info[4][1],
                                                self._ih[self._slot_offset] & 0x80))
        return

    def _validate_band(self, d, S):
        print("validate_band: {} {} {}".format(self._band.get(), d, S))
        if d != "1":
            return True
        if S.isdigit() and int(self._band.get() + S) < 6:
            return True
        """
        # doing this breaks verification
        slot_band = self._ih[self._slot_offset+1]
        self._entry_band.delete(0, END)
        self._entry_band.insert(0, str(slot_band))
        """
        print("{:X}] band: rejecting {} {}".format(self._slot, d, S))
        return False

    def _update_band(self, event=None):
        band = self._band.get()
        # FIXME: hard coded range check
        if band.isdigit() and int(band) < 6:
            slot_band = int(band)
            self._ih[self._slot_offset+1] = slot_band
        else:
            slot_band = self._ih[self._slot_offset+1]
        self._entry_band.delete(0, END)
        self._entry_band.insert(0, str(slot_band))
        print("{:X}] {} = {}".format(self._slot, self._var_info[4][2],
                                                self._ih[self._slot_offset+1]))
        return

    def _validate_frequency(self, d, S):
        print("validate_frequency: {} {} {}".format(self._frequency.get(), d, S))
        if d != "1":
            return True
        slot_freq_str = self._frequency.get() + S
        if slot_freq_str.isdigit() and int(slot_freq_str) < 300000000:
            return True
        """
        # doing this breaks verification
        val = self._frequency.get()
        if val == "":
            start = self._slot_offset + 4
            finish = start + 4
            t = 0
            # LSB first, reverse parse to simplify math
            for i in range(finish-1, start-1, -1):
                t = t * 256 + self._ih[i]
            slot_freq = str(t)
            self._entry_frequency.delete(0, END)
            self._entry_frequency.insert(0, str(slot_freq))
        """
        print("{:X}] frequency: rejecting {} {}".format(self._slot, d, S))
        return False

    def _update_frequency(self, event=None):
        slot_freq = int(self._frequency.get())
        # Integer LSB to MSB
        byts = []
        div = 1
        for i in range(0, 4):
            byts.append(int(slot_freq / div) % 256)
            div *= 256
        self._ih.frombytes(byts, self._slot_offset+4)
        self._entry_frequency.delete(0, END)
        self._entry_frequency.insert(0, str(slot_freq))
        print("{:X}] {} = {}".format(self._slot, self._var_info[4][3], slot_freq))
        return

    def _update_mode(self, event=None):
        mode = self._modelb.curselection()[0]
        active = self._ih[self._slot_offset] & 0x80
        self._ih[self._slot_offset] = (mode & 0x7F) | active
        print("{:X}] {} = {}".format(self._slot, self._var_info[4][4],
                                                    self._var_info[3][str(mode)]))
        return

    def _validate_pm(self, d, S):
        print("validate_pm: {} {} {}".format(self._pm.get(), d, S))
        if d != "1":
            return True
        if S.isdigit():
            modes = self._var_info[3]
            slot_mode_str = str(self._ih[self._slot_offset] & 0x7F)
            if modes[slot_mode_str].startswith("WSPR"):
                # power mode
                if self._pm.get() == "" and int(S) < 6:
                    return True
                if int(self._pm.get() + S) in self._dBm_powers:
                    return True
            else:
                # message mode
                if int(self._pm.get() + S) < 100:
                    return True
        print("{:X}] pm: rejecting {} {}".format(self._slot, d, S))
        return False

    def _update_pm(self, event=None):
        modes = self._var_info[3]
        slot_mode_str = str(self._ih[self._slot_offset] & 0x7F)
        if modes[slot_mode_str].startswith("WSPR"):
            slot_pm = int(self._pm.get())
            # power mode
            if slot_pm in self._dBm_powers:
                self._ih[self._slot_offset+3] = slot_pm
            else:
                slot_pm = self._ih[self._slot_offset+3]
        else:
            # message mode
            slot_pm = int(self._pm.get())
            if slot_pm < 100:
                self._ih[self._slot_offset+3] = slot_pm
            else:
                slot_pm = self._ih[self._slot_offset+3]
        self._entry_pm.delete(0, END)
        self._entry_pm.insert(0, str(slot_pm))
        print("{:X}] {} = {}".format(self._slot, self._var_info[4][5],
                                                    self._ih[self._slot_offset+3]))
        return

    def _validate_aux(self, d, S):
        print("validate_aux: {} {} {}".format(self._aux.get(), d, S))
        if d != "1":
            return True
        slot_aux_str = self._aux.get() + S
        if slot_aux_str.isdigit() and int(slot_aux_str) < 16:
            return True
        print("{:X}] aux: rejecting {} {}".format(self._slot, d, S))
        return False

    def _update_aux(self, event=None):
        slot_aux_str = self._aux.get()
        if slot_aux_str.isdigit() and int(slot_aux_str) < 16:
            self._ih[self._slot_offset+2] = int(slot_aux_str)
        else:
            slot_aux_str = "{}".format(self._ih[self._slot_offset+2])
            self._entry_aux.delete(0, END)
            self._entry_aux.insert(0, slot_aux_str)
        print("{:X}] {} = {}".format(self._slot, self._var_info[4][6],
                                                    self._ih[self._slot_offset+2]))
        return

class NewPageFrame(Frame):

    colour = ['green', 'red', 'blue' ]

    def __init__(self, parent, owner, page_name):
        Frame.__init__(self, parent)

        self.grid(row=0, column=0, sticky="nsew") # stick it to the _root_page
        # push away from the edges
        self.grid_columnconfigure(0, weight=1)
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(2, weight=1)
        self.grid_rowconfigure(2, weight=1)

        idx = owner.next_page_idx()
        self.configure(background=self.colour[(idx % len(self.colour))])

        # draw our button on our owner, not us
        msg = "{}".format(page_name)
        #button1 = Button(owner, text=msg, command=lambda : owner.show_page_frame(idx))
        button1 = Button(owner, text=msg, command=self.show)
        button1.grid(row=0, column=idx)
        button1.configure(background=self.colour[(idx % len(self.colour))])
        button1.configure(foreground='white')

        self._on_raise_command = None
        # track used rows
        self._row = 0
        return

    def next_row_idx(self):
        row = self._row
        self._row += 1
        return row

    def on_raise(self, command):
        self._on_raise_command = command
        return

    def show(self):
        self.tkraise()
        if self._on_raise_command:
            self._on_raise_command()
        return

class EEPedit(Frame):

    # built-in default charset (space and newline omitted)
    charset = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
               'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
               '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
               '/', '+', '-', '?', '.', '\'', '=', ')', '(', ':', '*', '#']

    digitset = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']

    def __init__(self, parent, ih, definitions):
        Frame.__init__(self, parent, background='grey')
        self.grid(row=0, column=0, sticky="nsew") # stick it to the root window frame

        self._page_idx = 0
        self._page_frame = []

        self._root_page = Frame(self)
        # Page Buttons placed on row 0
        self._root_page.grid(row=1, column=0, sticky="nsew")
        self.configure(background='white')
        #self._root_page.grid_columnconfigure(0, weight=1) # full window width
        #self._root_page.grid_rowconfigure(0, weight=1) # full window height

        # slots and messages create their own page and subframes
        # create page for settings
        self.new_page('Settings')
        self._settings_frame = self._page_frame[0]

        """
        fr = self._page_frame[0]
        fr.grid(sticky='nswe')

        # create subframe for settings
        self._settings_frame = NewPageFrame(fr, self)
        self._settings_frame.configure(background='yellow')
        # use middle column
        self._settings_frame.grid(column=1)
        self._settings_frame.grid_columnconfigure(2, weight=1)
        self._settings_frame.grid_rowconfigure(2, weight=1)
        """

        # grid(column, columnspan, in, ipadx, ipady, padx, pady, row, rowspan, sticky)
        # .grid_propagate(True)
        # .grid(row=fr.row, column=0, columnspan=1, sticky='nswe')
        # .place(in_=fr, anchor="c", relx=.5, rely=.5) # centre

        self.ih = ih

        #ihdict = self.ih.todict()     # dump contents to pydict
        #self.ih.dump()

        # Get the loaded EEP version string
        try:
            # This is dummy (in U3S), does it need to be considered ?
            # and should it always be zero ?
            # val = self.get_integer(self.ih, [TYPINT0, 0, 2])
            version = str(self.get_integer(self.ih, [TYPINT0, 4, 4]))
            if not version in definitions:
                msg = "Version {}\nNo Definition Found".format(version)
                messagebox.showerror('Definitions', msg)
                parent.exit()
                # notreached
                return
        except Exception as E:
                messagebox.showerror('Definitions', E)
                parent.exit()
                return

        self.definitions = definitions
        self.definition = self.definitions[version]
        #self.row = 0

        self.parse()
        return

    def exit(self):
        for p in self._page_frame:
            p.destroy()
        self.destroy()
        return

    def next_page_idx(self):
        page_idx = self._page_idx
        self._page_idx +=1
        return page_idx

    def new_page(self, page_name):
        frame = NewPageFrame(self._root_page, self, page_name)
        self._page_frame.append(frame)
        # support another page button
        self._root_page.grid(columnspan=self._page_idx)
        return frame

    # Notused
    def show_page_frame(self, idx):
        if idx >= len(self._page_frame):
            idx = 0
        #self._page_frame[idx].tkraise()
        self._page_frame[idx].show()
        return

    def get_integer(self, ih, parms):
        # Integer LSB to MSB
        start = parms[1]
        finish = start + parms[2]
        t = 0
        # LSB first, reverse parse to simplify math
        for i in range(finish-1, start-1, -1):
            t = t * 256 + ih[i]
        return t

    def get_compound(self, ih, var_info):
        s = []
        t = self.get_integer(ih, var_info)
        r = [t]
        for base in var_info[3]:
            s.append(int(r[len(r)-1] / base))
            r.append(t % base)
        s.append(r[len(r)-1])
        return s

    def new_subframe(self, frame):
        # drop the label text
        #fr = LabelFrame(frame, text=x, labelanchor='n')
        fr = LabelFrame(frame, foreground='white', background='grey')
        fr.grid(row=frame.next_row_idx(), column=1)
        return fr

    def text_reload(self):
        msg_data = self.ih.getsz(self._message_ofst)
        # convert 0xFF to our printable representation
        text_data = msg_data.replace(bytes([255]), bytes([ord('|')])).decode('ascii')
        self._message_text.delete(1.0, END)
        self._message_text.insert(END, text_data, ("r"))
        self.text_submsg_tag()
        return

    def text_submsg_tag(self):
        # tag all occurrences '|' as "submsg"
        start = '1.0'
        pos = self._message_text.search('|', start, stopindex=END)
        while pos:
            self._message_text.tag_add("submsg", pos)
            row, col = pos.split('.')
            start = row + '.' + str(int(col) + 1)
            pos = self._message_text.search('|', start, stopindex=END)
        return

    def text_submsg(self):
        self._message_text.insert(INSERT, '|', ("submsg"))
        return

    def text_save(self):
        mistrake = False
        # upper copy, throw away the forced last '\n' character
        text_data = self._message_text.get(1.0, "end-1c").upper()
        # truncate length as required
        if len(text_data) >= self._message_len:
            text_data = text_data[:self._message_len-2]
            mistrake = True
        self._message_text.delete(1.0, END)
        # reload text applying syntax highlighting
        for c in text_data:
            if c in self.charset or c == ' ':
                tag = "n"
            elif c == '|':
                tag = "submsg"
            else:
                tag = "mistrake"
                mistrake = True
            self._message_text.insert(END, c, (tag))
        if mistrake:
            return

        # convert our printable representation '|' to 0xFF
        msg_data = text_data.replace('|', chr(255))
        # write to intelhex buffer
        self.ih.putsz(self._message_ofst, msg_data)
        # reload text applying syntax highlighting
        self._message_text.delete(1.0, END)
        self._message_text.insert(END, text_data, ("r"))
        self.text_submsg_tag()
        return

    def do_textbox(self, frame, x):
        var_info = self.definition[x]
        self._message_ofst = var_info[1]
        self._message_len = var_info[2]

        # get a resizing frame to hold textbox and buttons
        fr = self.new_subframe(frame)
        # centre the textbox resizing frame
        fr.place(in_=frame, anchor="c", relx=.5, rely=.5) # centre

        self._message_text = Text(fr, width=32, height=16)
        self._message_text.tag_config("n", background="white", foreground="black")
        self._message_text.tag_config("r", background="black", foreground="white")
        self._message_text.tag_config("mistrake", background="red", foreground="yellow")
        self._message_text.tag_config("submsg", background="yellow", foreground="red")
        #ScrollBar = Scrollbar(fr)
        #ScrollBar.config(command=self._message_text.yview)
        #self._message_text.config(yscrollcommand=ScrollBar.set)
        #ScrollBar.grid(row=0, column=1)
        self._message_text.grid(row=0, column=0, columnspan=3, sticky='nsew')
        self.text_reload()

        b_reload = Button(fr, text='Reload', height=1, width=5,
            activeforeground='red', command=self.text_reload)
        b_reload.grid(row=1, column=0)
        b_reload.configure(state='normal')

        b_reload = Button(fr, text='SubMsg', height=1, width=5,
            activeforeground='red', command=self.text_submsg)
        b_reload.grid(row=1, column=1)
        b_reload.configure(state='normal')

        b_save = Button(fr, text='Submit', height=1, width=5,
            activeforeground='red', command=self.text_save)
        b_save.grid(row=1, column=2)
        b_save.configure(state='normal')
        # take focus when our page is raised
        frame.on_raise(self._message_text.focus_set)
        return

    def do_asciisz(self, frame, x):
        var_info = self.definition[x]

        fr = self.new_subframe(frame)

        val = self.ih.getsz(var_info[1]).decode('ascii')[:var_info[2]-1]
        vlabel = Label(fr, text=var_info[3], foreground='black', background='grey')
        vlabel.grid(row=0, column=0)
        wid = min(var_info[2], 25)
        var = StringVar()
        entry_str = Entry(fr, textvariable=var, width=wid, justify=RIGHT,
                                            foreground='yellow', background='grey')
        porknham = up_asciisz(var, self.ih, var_info, entry_str, self.charset)
        entry_str.bind('<Key-Return>', porknham.do_event)
        entry_str.grid(row=0, column=1)
        entry_str.insert(0, val)
        return

    def do_integer(self, frame, x):
        var_info = self.definition[x]

        fr = self.new_subframe(frame)

        vlabel = Label(fr, text=var_info[3], foreground='black', background='grey')
        vlabel.grid(row=0, column=0)
        wid = min((2**var_info[2]), 10)

        var = StringVar()
        porknham = up_int(var, self.ih, var_info, self.digitset)
        e_str = Entry(fr, textvariable=var, width=wid, justify=RIGHT)
        e_str.bind('<Key-Return>', porknham.do_event)
        e_str.grid(row=0, column=2)

        val = str(self.get_integer(self.ih, var_info))
        e_str.insert(0, val)
        return

    def do_compound(self, frame, x):
        var_info = self.definition[x]
        r = self.get_compound(self.ih, var_info)

        fr = self.new_subframe(frame)
        fr.configure(text=var_info[5], labelanchor='n')

        varlist = []

        wid = min(int(36 / len(r)), 10)
        for i in range(0, len(r)):
            varlist.append(StringVar())
            vlabel = Label(fr, text=var_info[4][i], foreground='black', background='grey')
            vlabel.grid(row=0, column=(i*2), sticky='nswe')
            porknham = up_compound(i, varlist, self.ih, var_info, self.digitset)
            e_str = Entry(fr, textvariable=varlist[i], width=wid, justify=RIGHT)
            e_str.bind('<Key-Return>', porknham.do_event)
            e_str.grid(row=0, column=(i*2)+1)
            e_str.insert(0, r[i])
        return

    def do_slot(self, frame, x):
        fr = self.new_subframe(frame)
        # centre the slot subframe
        fr.place(in_=frame, anchor="c", relx=.5, rely=.5) # centre

        var_info = self.definition[x]
        llist = var_info[4]
        for i in range(0, len(llist)):
            self.slot_label(i, llist[i], fr)

        self.slotrow = []
        # FIXME: extract number of slots
        for slot in range(0, 16):
            self.slotrow.append(SlotRow(self.ih, var_info, slot, fr))
        return

    def slot_label(self, idx, label, fr):
        label = Label(fr, text="{}".format(label), foreground='black', background='grey')
        label.grid(row=0, column=idx)

    def parse(self):
        for x in sorted(self.definition):
            var_info = self.definition[x]
            if var_info[0] == TYPVER0:
                print("{}: {}".format(var_info[3], self.get_integer(self.ih, var_info)))
            elif var_info[0] == TYPCHR0:
                print("{}: {}".format(var_info[2], var_info[1]))
                self.charset = var_info[1]
            elif var_info[0] == TYPINT0:
                print("{}: {}".format(var_info[3], self.get_integer(self.ih, var_info)))
                self.do_integer(self._settings_frame, x)
            elif var_info[0] == TYPTXZ0:
                val = self.ih.getsz(var_info[1])
                textstr = val.replace(bytes([255]), bytes([ord('|')])).decode('ascii')
                print("{}: {}".format(var_info[3], textstr))
                if var_info[2] > 50:
                    self.do_textbox(self.new_page(var_info[3]), x)
                else:
                    self.do_asciisz(self._settings_frame, x)
            elif var_info[0] == TYPCPD0:
                s = "{}".format(var_info[5])
                r = self.get_compound(self.ih, var_info)
                for i in range(0, len(r)):
                    s += " {}:{}".format(var_info[4][i], str(r[i]))
                print(s)
                """
                s = " ".join(str(v) for v in self.get_compound(self.ih, var_info))
                print("{}: {}".format(x, s))
                """
                self.do_compound(self._settings_frame, x)
            elif var_info[0] == TYPSLT0:
                s = "{}".format(var_info[4][0])
                for i in range(1, len(var_info[4])):
                    s += "  {} ".format(var_info[4][i])
                print(s)
                # FIXME: extract number of slots
                for slot in range(0, 16):
                    slot_offset = var_info[1] + (slot * 8)
                    # Slot#
                    msg = "{:X}]".format(slot)
                    # Active
                    if self.ih[slot_offset] & 0x80:
                        msg += '        '
                    else:
                        msg += '   ---  '
                    # Band
                    msg += "{:3d}    ".format(self.ih[slot_offset+1])
                    # Frequency
                    start = slot_offset + 4
                    finish = start + 4
                    t = 0
                    # LSB first, reverse parse to simplify math
                    for i in range(finish-1, start-1, -1):
                        t = t * 256 + self.ih[i]
                    msg += "{:10d}   ".format(t)
                    modes = var_info[3]
                    # Mode, need a string for key lookup
                    slot_mode_str = str(self.ih[slot_offset] & 0x7F)
                    if slot_mode_str in modes:
                        msg += "{:7} ".format(modes[slot_mode_str])
                        # Power or Message Index (redundant....)
                        if modes[slot_mode_str].startswith("WSPR"):
                            # Power
                            msg += "{:2d}    ".format(self.ih[slot_offset+3])
                        else:
                            # Message Index
                            msg += "{:2d}    ".format(self.ih[slot_offset+3])
                    else:
                        msg += "{:7} ".format("UNDEF")
                        # Either Power or Message Index
                        msg += "{:2d}    ".format(self.ih[slot_offset+3])
                    # Auxiliary
                    msg += "{:X}".format(self.ih[slot_offset+2])
                    print(msg)

                self.do_slot(self.new_page(var_info[5]), x)

            else:
                print("{}:{}".format(x, var_info))
        # raise 'Settings' page to top
        #self.show_page_frame(0)
        self._settings_frame.show()
        return
        

class EEPeditor(Tk):

    def __init__(self, *args, **kwargs):
        Tk.__init__(self, *args, **kwargs)
        self.wm_title('EEPeditor')
        #self.geometry("300x300")
        self.makemenu()
        self._current_file = None
        self.definitions = None
        self.load_definitions()
        return

    def exit(self):
        """ exit, tidy up after GUI destruction """
        self.destroy()
        sys.exit(0)
        return

    def showHelp(self):
        helpmsg = "\n\
\nTo change Slot Mode\n\
Mouse select, cursor Up/Down\n\
\nAfter changing ANY field\n\
Press 'Enter' to register\n\
"
        messagebox.showinfo('Help', helpmsg)
        return

    def showAbout(self):
        aboutmsg = "\
       EEPeditor " + str(VERSION) + "\n\
\n\
   Copyright (C) 2018\n\
        Gray Remlin\n\
<gryrmln@gmail.com>\n\
"
        messagebox.showinfo('About EEPeditor', aboutmsg)
        return

    def load_definitions(self):
        deffile = os.path.join(os.path.dirname(__file__), 'eep_defs.json')
        try:
            with open(deffile, 'r') as f:
                definitions = json.load(f)
        except FileNotFoundError:
            messagebox.showerror('Definitions', 'No definitions file!')
        except IOError:
            messagebox.showerror('Definitions', 'Opening definitions failed!')
        except EOFError:
            messagebox.showerror('Definitions', 'Loading definitions failed!')
        except Exception as E:
            print("{}".format(E))
            self.exit()
        else:
            if len(definitions) == 0:
                messagebox.showerror('Definitions', 'No definitions found!')
                return
            for v in definitions:
                print("{}".format(v))
            self.definitions = definitions
            self.m_file.entryconfig(0, state=NORMAL)
        return

    def load_eep(self, filename=None):
        if filename == None:
            ttl = 'Load EEP File'
            ft = (("EEP Files", "*.eep"), ("All files", "*.*"))
            filename = filedialog.askopenfilename(defaultextension='.eep',
                                                  title=ttl, filetypes=ft)
        if filename:
            """
            fname, fext = os.path.splitext(filename)
            if fext.upper() == '.EEP':
            """
            try:
                self.ih = IntelHex(filename)
                self._current_file = filename
            except:
                messagebox.showerror('File Error', 'File "{}"!'.format(filename))
                return

            self.m_file.entryconfig(0, state=DISABLED)
            self.m_file.entryconfig(1, state=NORMAL)
            self.m_file.entryconfig(2, state=NORMAL)
            self.ee = EEPedit(self, self.ih, self.definitions)
        return

    def close_eep(self):
        self.m_file.entryconfig(2, state=DISABLED)
        self.m_file.entryconfig(1, state=DISABLED)
        self.m_file.entryconfig(0, state=NORMAL)
        self.ee.exit()
        return

    def save_eep(self, filename=None):
        #messagebox.showerror('File Save', 'Not Implemented!')
        #self._ih.tofile("eep_editor.eep", 'hex')
        ttl = 'Save EEP File'
        ft = (("EEP Files", "*.eep"), ("All files", "*.*"))
        if filename == None:
            filename = self._current_file
        if filename == None:
            filename = filedialog.asksaveasfilename(defaultextension='.eep',
                                                    title=ttl, filetypes=ft)
        else:
            filename = filedialog.asksaveasfilename(initialfile=filename,
                                                    defaultextension='.eep',
                                                    title=ttl, filetypes=ft)
        if filename:
            try:
                #self.ih.tofile(filename, 'hex')
                self.ih.write_hex_file(filename,  eolstyle='CRLF')
            except:
                messagebox.showerror('File Error', 'File "{}" !'.format(filename))
        return

    def makemenu(self):
        top = Menu(self)
        self.config(menu=top)    # set its menu option
        self.m_file = m_file = Menu(top, tearoff=0)
        top.add_cascade(label='File', menu=m_file, underline=0)
        m_file.add_command(label='Load', command=self.load_eep,
                           underline=0, accelerator='Ctrl+L')
        m_file.add_command(label='Save', command=self.save_eep,
                           underline=0, accelerator='Ctrl+S')
        m_file.add_command(label='Close', command=self.close_eep,
                           underline=0, accelerator='Ctrl+C')
        m_file.add_command(label='Exit', command=self.exit,
                           underline=1, accelerator='Ctrl+X')
        opt = Menu(top, tearoff=0)
        top.add_cascade(label='Preferences', menu=opt, underline=0)
        _help = Menu(top, tearoff=0)
        top.add_cascade(label='Help', menu=_help, underline=0)
        _help.add_command(label='Controls', command=self.showHelp, underline=0)
        _help.add_command(label='About', command=self.showAbout, underline=0,
                                                                accelerator='Ctrl+A')
        m_file.entryconfig(0, state=DISABLED)
        m_file.entryconfig(1, state=DISABLED)
        m_file.entryconfig(2, state=DISABLED)
        m_file.entryconfig(3, state=NORMAL)
        return


def main():

    try:
        app = EEPeditor()
        argc = len(sys.argv)
        if argc == 2:
            app.load_eep(sys.argv[1])
        app.mainloop()
    except KeyboardInterrupt:
        sys.exit()
    finally:
        print("Goodbye")

if __name__ == '__main__':
    main()
 
