#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim:ai:sw=4:ts=8:et:fileencoding=utf-8
#
# EEP Defs 1v2
#
# Copyright (C) 2018 Gray Remlin 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import json
from eep_consts import *

# NOTE: integer keys are stored as a string by JSON
definitions = {
    # U3 v3.09f
    2270940443:{
    '0':[TYPCHR0, ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
              'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
              '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
              '/', '+', '-', '?', '.', '\'', '=', ')', '(', ':', '*', '#'], 'CharSet'],
    'A':[TYPVER0, 4, 4, 'Version'],
    'B': [TYPTXZ0, 8, 7, 'Locator'],
    'C': [TYPTXZ0, 15, 15, 'Callsign'],
    'D': [TYPCPD0, 30, 4, [100], ['Frame', 'Start'], 'Frame Timing'],
    'E': [TYPCPD0, 34, 4, [1000000], ['Mode', 'Baud'], 'Gps Settings'],
    'F': [TYPCPD0, 38, 4, [10], ['Info', 'Check'], 'Gps Options'],
    'P':[TYPSLT0, 42, 128,
        {0:'None', 1:'FSKCW', 2:'QRSS', 3:'DFCW', 4:'SHELL', 5:'HELL',
        6:'DXHELL', 7:'CW', 8:'CWID', 9:'FSK', 10:'WSPR', 11:'WSPR15', 
        12:'TX_CW', 13:'TX_FSK', 14:'OPERA05', 15:'OPERA1', 16:'OPERA2', 
        17:'OPERA4', 18:'OPERA8', 19:'OPERA32', 20:'OPERA65', 21:'OPERA2H', 
        22:'PI4', 23:'JT9-1', 24:'JT9-2', 25:'JT9-5', 26:'JT9-10', 27:'JT9-30', 
        },
        ['Idx', 'Act', 'Band', 'Frequency', 'Mode', 'P/M', 'Aux'], 'Slots'],
    'G': [TYPINT0, 170, 4, 'Ref Frq. (DDS Only)'],
    'H':[TYPINT0, 174, 4, 'RefClk Hz (OCG Only)'],
    'I': [TYPINT0, 178, 4, 'SysClk Hz'],
    'J': [TYPCPD0, 182, 4, [100000, 10000, 1000, 100], ['XW', 'x2', 'Tn', 'Iv', 'TxS'], 'Configuration'],
    'K': [TYPCPD0, 186, 4, [1000000, 1000], ['CW', 'Dit', 'Hel', 'Speed'], 'Speed'],
    'L': [TYPCPD0, 190, 4, [1000], ['Fine', 'Hz'], 'Fsk'],
    'M': [TYPCPD0, 194, 4, [1000000000], ['Mode', 'Freq'], 'Park'],
    'N': [TYPCPD0, 198, 4, [1000], ['Step', 'Time'], 'Calibration'],
    'O': [TYPINT0, 202, 4, 'Backlight'],
    'Q': [TYPTXZ0, 206, 512, 'Messages']
    },

    # U3S v3.12a
    2270940446:{
    '0':[TYPCHR0, ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
              'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
              '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
              '/', '+', '-', '?', '.', '\'', '=', ')', '(', ':', '*', '#'], 'CharSet'],
    'A':[TYPVER0, 4, 4, 'Version'],
    'B':[TYPTXZ0, 8, 7, 'Locator'],
    'C':[TYPTXZ0, 15, 15, 'Callsign'],
    'D':[TYPCPD0, 30, 4, [100],['Frame', 'Start'], 'Frame Timing'],
    'E':[TYPCPD0, 34, 4, [1000000], ['Mode', 'Baud'], 'Gps Settings'],
    'F':[TYPCPD0, 38, 4, [10], ['Info', 'Check'], 'Gps Options'],
    'Q':[TYPSLT0, 42, 128,
        {0:'RX', 1:'FSKCW', 2:'QRSS', 3:'DFCW', 4:'SHELL', 5:'HELL',
        6:'DXHELL', 7:'CW', 8:'CWID', 9:'FSK', 10:'WSPR', 11:'WSPR15', 
        12:'TX_CW', 13:'TX_FSK', 14:'OPERA05', 15:'OPERA1', 16:'OPERA2', 
        17:'OPERA4', 18:'OPERA8', 19:'OPERA32', 20:'OPERA65', 21:'OPERA2H', 
        22:'PI4', 23:'JT9-1', 24:'JT9-2', 25:'JT9-5', 26:'JT9-10', 27:'JT9-30', 
        28:'ISCATA', 29:'ISCATB', 30:'JT65A', 31:'JT65B', 32:'JT65C',
        },
        ['Idx', 'Act', 'Band', 'Frequency', 'Mode', 'P/M', 'Aux'], 'Slots'],
    'G':[TYPINT0, 170, 4, 'RefClk Hz'],
    'H':[TYPINT0, 174, 4, 'SysClk Hz'],
    'I':[TYPCPD0, 178, 4, [100000, 10000, 1000, 100], ['XW', 'x2', 'Tn', 'Iv', 'TxS'], 'Configuration'],
    'J':[TYPCPD0, 182, 4, [1000000, 1000], ['CW', 'Dit', 'Hel', 'Speed'], 'Speed'],
    'K':[TYPCPD0, 186, 4, [1000], ['Fine', 'Hz'], 'Fsk'],
    'L':[TYPCPD0, 190, 4, [1000000000], ['Mode', 'Freq'], 'Park'],
    'M':[TYPINT0, 194, 4, 'Ptt'],
    'N':[TYPCPD0, 198, 4, [1000],['mS', 'Max'], 'Raised Cosine'],
    'O':[TYPCPD0, 202, 4, [1000], ['Step', 'Time'], 'Calibration'],
    'P':[TYPCPD0, 206, 4, [1000], ['Bright', 'Time'], 'Backlight'],
    'R':[TYPTXZ0, 210, 512, 'Messages']
    }
}

def save_definitions(filename=None):
    global definitions
    if not filename:
        filename = 'eep_defs.json'
    print("Saving to \"{}\"".format(filename))
    try:
        with open(filename, 'w') as f:
            json.dump(definitions, f)
    except Exception as E:
        print("{}".format(E))
    return

def main():
    print("EEP Defs {}".format(VERSION))
    filename = None
    argc = len(sys.argv)
    if argc == 2:
      fname, fext = os.path.splitext(sys.argv[1])
      if fext.upper() == '.JSON':
            filename = sys.argv[1]
    save_definitions(filename)
    sys.exit()

if __name__ == '__main__':
    main()

