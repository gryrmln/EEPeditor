Installing gryrmln/EEPeditor
============================

<br>

EEPeditor has the following dependancies:

Python3

Python3 Tk/Tcl support

Python3 IntelHex support

<br>

**Install dependency Python3**

    > sudo apt-get install python3


**Install dependency Python3 Tk/Tcl**

    > sudo apt-get install python3-tk


**Install dependency Python3 IntelHex**

*IntelHex can be either installed via use of:*

  * Python Package Manager 'pip' (recommended)
  * Git
  * Direct download Zip file


*Install dependency Python3 IntelHex using pip*

Install pip

    > sudo apt-get install python3-pip

Install IntelHex

    > sudo python3 -m pip install intelhex


*Install dependency Python3 IntelHex using Git*

Install git

    > sudo apt-get install git

Install IntelHex

    > cd

    > git clone https://github.com/bialix/intelhex.git intelhex.git

    > cd intelhex.git

    > sudo python3 setup.py install


*Install dependency Python3 IntelHex using zip download*

Navigate to the following URL

    https://github.com/bialix/intelhex.git

    and click on 'Clone or Download' then 'ZIP'

After unzipping

    > cd intelhex.git

    > sudo python3 setup.py install


**Install EEPeditor**

*EEPeditor can be either installed via use of:*

  * Git (recommended)
  * Direct download Zip file


*Install Python3 EEPeditor using git*

Install git

    > sudo apt-get install git

Install Python3 EEPeditor

    > git clone https://github.com/gryrmln/EEPeditor.git EEPeditor.git


*Install Python3 EEPeditor using zip download*

Navigate to the following URL

    https://github.com/gryrmln/EEPeditor.git

    and click on 'Clone or Download' then 'ZIP'

After unzipping


**To run EEPeditor**

    > cd EEPeditor.git

    > ./eep_editor.py 'optional eep filename'

optionally associate '.eep' files with eep_editor.py in your chosen file manager


*To download latest EEPeditor Updates using git*

    > cd  EEPeditor.git

    > git pull


